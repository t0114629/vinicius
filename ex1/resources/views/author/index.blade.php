<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Authors</title>
 
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
 
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">


  </head>
<body>
    
    <table class="table table-dark">
        <thead>
          <tr>
            <th scope="col">ID</th>
            <th scope="col">Nome</th>
            <th scope="col">Nacionalidade</th>
            <th scope="col">CRUD</th>
          </tr>
        </thead>
        <tbody>
     @foreach($authors as $author)
          <tr>
          <th scope="row">{{$author->id}}</th>
          <td>{{$author->name}}</td>
          <td>{{$author->nacionality}}</td>
          <td>    
              
          <a style="float:left"href="{{route('author.edit', $author->id)}}"><button type="button" class="btn btn-warning">Editar</button></a>
          <form action="{{ route('author.destroy', $author->id)}}" method="post">
              @method('DELETE')
              @csrf
              <input class="btn btn-danger" type="submit" value="Delete" />
           </form>  
          </td>
          </tr>
          @endforeach    
    
        </tbody>
      </table>
      <a href="{{route('author.create')}}">
          <button type="button" class="btn btn-success" style="padding:15px;font-size:16px;">Adicionar</button>
      </a>  

</body>
</html>