<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Authors</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>
<body>
    <div style="text-align:center;">

          
           <form method="post" action="{{route('author.store')}}">                 
             @csrf
                <div class="form-group" style="margin-left:25%;margin-right:25%;">
                  <label for="exampleInputEmail1">Name</label>
                <input type="text" class="form-control" name="name" aria-describedby="emailHelp" placeholder="Enter Name">
                  
                </div>
                <div class="form-group" style="margin-left:25%;margin-right:25%;">
                  <label for="exampleInputPassword1">Nacionality</label>
                  <input type="text" class="form-control" name="nacionality" placeholder="Enter Nacionality">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
              </form>
      
    </div>
</body>
</html>